import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateServiceStub } from './../../test/stub-translate.service';
import { AngularMaterialModule } from './../angular-material.module';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameComponent } from './name.component';
import { By } from '@angular/platform-browser';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

describe('NameComponent', () => {
  let component: NameComponent;
  let fixture: ComponentFixture<NameComponent>;

  const mockRouter = { navigate: jasmine.createSpy('navigate') };
  const mockActivatedRoute = ({} as any) as ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [FormsModule, TranslateModule, RouterModule, BrowserAnimationsModule, AngularMaterialModule],
    declarations: [ NameComponent ],
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      {
        provide: TranslateService,
        useClass: TranslateServiceStub
      }
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // Verify
    expect(component).toBeTruthy();
  });

  it('play button should be disabled', () => {
    // Verify
    const playButton = fixture.debugElement.query(By.css('button.play-button'));
    expect(playButton.nativeElement.getAttribute('disabled')).toEqual('');
  });

  it('play button should be enabled', () => {
    // Prepare
    component.username = 'myName';
    fixture.detectChanges();

    // Verify
    const playButton = fixture.debugElement.query(By.css('button.play-button'));
    expect(component.username).toEqual('myName');
    expect(playButton.nativeElement.getAttribute('disabled')).toEqual(null);
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NameComponent } from './name/name.component';
import { PlayComponent } from './play/play.component';

const routes: Routes = [
  { path: '', redirectTo: '/name', pathMatch: 'full' },
  { path: 'name', component: NameComponent },
  { path: 'play', component: PlayComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

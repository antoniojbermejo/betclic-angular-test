import { NgModule } from '@angular/core';
import {MatInputModule, MatButtonModule, MatSnackBarModule} from '@angular/material';

@NgModule({
  imports: [MatInputModule, MatButtonModule, MatSnackBarModule],
  exports: [MatInputModule, MatButtonModule, MatSnackBarModule],
})
export class AngularMaterialModule { }

export enum Winner {
    PLAYER = 'player',
    COMPUTER = 'computer',
    DRAW = 'draw'
}

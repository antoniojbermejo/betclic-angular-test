export enum Item {
    ROCK = 'rock',
    PAPER = 'paper',
    SCISSORS = 'scissors'
}

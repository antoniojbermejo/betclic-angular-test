import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Item } from '../shared/enum/Item';
import { Winner } from '../shared/enum/Winner';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  name: string;

  NB_GAMES_MAX = 5;
  ITEMS = [Item.ROCK, Item.PAPER, Item.SCISSORS];

  nbGamesLeft: number;
  playerScore: number;
  computerScore: number;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public snackBar: MatSnackBar,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this._reset();

    // If error or no username, redirection to the 'name' route
    this.activatedRoute.queryParams.subscribe(
      params => {
        this.name = params['name'];
        if (!this.name) {
          this.router.navigate(['/name']);
        }
      },
      error => {
        this.router.navigate(['/name']);
      }
    );
  }

  /**
   * Play with the player's choice.
   * @param playersChoice item chosen by the player
   */
  play(playersChoice: Item) {
    this.nbGamesLeft--;

    // Simulate the computer choice
    const computersChoice = this._randomChoice();

    // Get the winner
    const winner = this._getWinner(playersChoice, computersChoice);
    if (winner === Winner.PLAYER) {
      this.playerScore++;
    } else if (winner === Winner.COMPUTER) {
      this.computerScore++;
    }
    this._displayWinner(computersChoice, winner);

  }

  _reset() {
    this.playerScore = 0;
    this.computerScore = 0;
    this.nbGamesLeft = this.NB_GAMES_MAX;
  }
  /**
   * Simulate the computer choice randomly.
   */
  _randomChoice(): Item {
    const computerChoice = Math.floor((Math.random() * 3));
    return this.ITEMS[computerChoice];
  }

  /**
   * Calculates the winning choice.
   * @param playersChoice the player's choice
   * @param computersChoice the computer's choice
   * @returns the winner
   */
  _getWinner(playersChoice: Item, computersChoice: Item): Winner {
    let winner = Winner.DRAW;

    if ((playersChoice === Item.ROCK && computersChoice === Item.PAPER)
      || (playersChoice === Item.PAPER && computersChoice === Item.SCISSORS)
      || (playersChoice === Item.SCISSORS && computersChoice === Item.ROCK)) {
      winner = Winner.COMPUTER;
    } else if ((playersChoice === Item.ROCK && computersChoice === Item.SCISSORS)
      || (playersChoice === Item.PAPER && computersChoice === Item.ROCK)
      || (playersChoice === Item.SCISSORS && computersChoice === Item.PAPER)) {
      winner = Winner.PLAYER;
    }

    return winner;
  }

  _displayWinner(computersChoice: string, winner: Winner) {
    this.translateService.get(`rockPaperScissors.play.win.${winner}`, {value: computersChoice})
    .subscribe(key => {
      this.snackBar.open(key, '', {
        duration: 1500,
      });
    });
  }

}

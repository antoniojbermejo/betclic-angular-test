import { MatSnackBarStub } from './../../test/stub-matsnackbar.service';
import { MatSnackBar } from '@angular/material';
import { TranslateServiceStub } from './../../test/stub-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';
import { Item } from '../shared/enum/Item';
import { Winner } from '../shared/enum/Winner';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  const mockRouter = { navigate: jasmine.createSpy('navigate') };
  const mockActivatedRoute = ({ queryParams: of({ name: 'username' }) } as any) as ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayComponent ],
      providers: [
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: TranslateService,
          useClass: TranslateServiceStub
        },
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute
        },
        {
          provide: MatSnackBar,
          useValue: MatSnackBarStub
        }
      ]
    })
    .overrideTemplate(PlayComponent, '')
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    // Verify
    expect(component).toBeTruthy();
  });

  it('ngOnInit should not redirect', () => {
    // Prepare
    const name = 'username';
    spyOn(mockActivatedRoute, 'queryParams').and.returnValue(
      of({ name })
    );

    // Execute
    component.ngOnInit();

    // Verify
    expect(component.playerScore).toEqual(0);
    expect(component.computerScore).toEqual(0);
    expect(component.nbGamesLeft).toEqual(component.NB_GAMES_MAX);
    expect(component.name).toEqual(name);

    expect(mockRouter.navigate).toHaveBeenCalledTimes(0);
  });

  it('_randomChoice should get a random Item', () => {
    // Execute
    const result = component._randomChoice();

    // Verify
    expect(component.ITEMS).toContain(result);
  });

  it('_getWinner should return PLAYER', () => {
    // Execute
    const result = component._getWinner(Item.PAPER, Item.ROCK);
    const result2 = component._getWinner(Item.ROCK, Item.SCISSORS);
    const result3 = component._getWinner(Item.SCISSORS, Item.PAPER);

    // Verify
    expect(result).toEqual(Winner.PLAYER);
    expect(result2).toEqual(Winner.PLAYER);
    expect(result3).toEqual(Winner.PLAYER);
  });

  it('_getWinner should return COMPUTER', () => {
    // Execute
    const result = component._getWinner(Item.PAPER, Item.SCISSORS);
    const result2 = component._getWinner(Item.ROCK, Item.PAPER);
    const result3 = component._getWinner(Item.SCISSORS, Item.ROCK);

    // Verify
    expect(result).toEqual(Winner.COMPUTER);
    expect(result2).toEqual(Winner.COMPUTER);
    expect(result3).toEqual(Winner.COMPUTER);
  });

  it('_getWinner should return DRAW', () => {
    // Execute
    const result = component._getWinner(Item.PAPER, Item.PAPER);
    const result2 = component._getWinner(Item.ROCK, Item.ROCK);
    const result3 = component._getWinner(Item.SCISSORS, Item.SCISSORS);

    // Verify
    expect(result).toEqual(Winner.DRAW);
    expect(result2).toEqual(Winner.DRAW);
    expect(result3).toEqual(Winner.DRAW);
  });

  it('_displayWinner should return DRAW', () => {
    // Execute
    const result = component._getWinner(Item.PAPER, Item.PAPER);
    const result2 = component._getWinner(Item.ROCK, Item.ROCK);
    const result3 = component._getWinner(Item.SCISSORS, Item.SCISSORS);

    // Verify
    expect(result).toEqual(Winner.DRAW);
    expect(result2).toEqual(Winner.DRAW);
    expect(result3).toEqual(Winner.DRAW);
  });

  it('play should decrease the nbGamesLeft ', () => {
    // Prepare
    spyOn(mockActivatedRoute, 'queryParams').and.returnValue(
      of({ name: 'username' })
    );
    // spyOn(snackBar, 'open');
    component.ngOnInit();

    // Execute
    component.play(Item.PAPER);

    // Verify
    expect(component.nbGamesLeft).toEqual(component.NB_GAMES_MAX - 1);
  });
});

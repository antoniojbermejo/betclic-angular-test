import { of } from 'rxjs';
import { EventEmitter } from '@angular/core';

export class TranslateServiceStub {

    public onLangChange: EventEmitter<any> = new EventEmitter();
    public onTranslationChange: EventEmitter<any> = new EventEmitter();
    public onDefaultLangChange: EventEmitter<any> = new EventEmitter();

    public setDefaultLang(lang: string): any {
        // Do nothing
    }
    public use(lang: string): any {
        // Do nothing
    }

    public get(key: any): any {
        return of(key);
    }
}
